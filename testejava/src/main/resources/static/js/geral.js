/* VARS */
var aoLanguage = {
    "sProcessing": "Processando...",
    "sLengthMenu": "Exibindo: _MENU_ resultados",
    "sZeroRecords": "Não foram encontrados resultados",
    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando de 0 até 0 de 0 registros",
    "sInfoFiltered": "(filtrado de _MAX_ registros no total)",
    "sInfoPostFix": "",
    "sSearch": "Pesquisar",
    "sUrl": "",
    "oPaginate": {
        "sFirst": "Primeiro",
        "sPrevious": "Anterior",
        "sNext": "Seguinte",
        "sLast": "Último"
    },
};

/* OMNIFACIL FUNCTION TO TEMPLATES 

REMOVE FOR WEB DEVELOPING

*/

function menuHeader() {
    $("#new-menu").load("/templates/geral/header.html", function (response, status, xhr) {

        if (status == "success") {

            $('.navbar a.dropdown-toggle').on('click', function (e) {
                var $el = $(this);
                var $parent = $(this).offsetParent(".dropdown-menu");
                $(this).parent("li").toggleClass('open');

                if (!$parent.parent().hasClass('nav')) {
                    $el.next().css({ "top": $el[0].offsetTop, "left": $parent.outerWidth() - 4 });
                }

                $('.nav li.open').not($(this).parents("li")).removeClass("open");

                return false;
            });
        }
    });
}

/*MENU TEMPLATES*/

var omni = omni || {};

/* GERAL */
omni.geral = omni.geral || {};

omni.geral = {

    menuHeader: function () {
        $("#new-menu").load("/templates/geral/header.html", function (response, status, xhr) {

            if (status == "success") {

                $('.navbar a.dropdown-toggle').on('click', function (e) {
                    var $el = $(this);
                    var $parent = $(this).offsetParent(".dropdown-menu");
                    $(this).parent("li").toggleClass('open');

                    if (!$parent.parent().hasClass('nav')) {
                        $el.next().css({ "top": $el[0].offsetTop, "left": $parent.outerWidth() - 4 });
                    }

                    $('.nav li.open').not($(this).parents("li")).removeClass("open");

                    return false;
                });
            }
        });
    },

    getdate: function (elem) {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        if (s < 10) {
            s = "0" + s;
        }

        $(elem).text(h + ":" + m + ":" + s);
        setTimeout(function () { omni.geral.getdate(elem) }, 1000);
    },

    sameHeight: function (elem) {
        var eleHeight = 0;

        $(elem).each(function () {
            if ($(this).height() > eleHeight) {
                eleHeight = $(this).height();
                //console.log(eleHeight);
            }
            if ($(document).width() < 991) {
                eleHeight = $(this).height('auto');
            }
        });

        $(elem).css('height', eleHeight);

    },

    menuHeader: function (elem) {
        elem = elem + '.navbar a.dropdown-toggle';

        $(elem).on('click', function (e) {
            var $el = $(this);
            var $parent = $(this).offsetParent(".dropdown-menu");

            $(this).parent("li").toggleClass('open');

            if (!$parent.parent().hasClass('nav')) {
                $el.next().css({ "top": $el[0].offsetTop, "left": $parent.outerWidth() - 4 });
            }

            $('.nav li.open').not($(this).parents("li")).removeClass("open");

            return false;
        });
    },

    /* SCROLLBAR NO ELEMENTO */
    scrollBars: function (elem, size) {
        $(elem).slimScroll({
            color: '#d6100d',
            height: size,
            railVisible: true,
            alwaysVisible: true
        });
    },

    /* CONTADOR DE CARACTERES */
    countChar: function (val, limit) {
        countTxt = '.' + val.id + 'Count span';
        var len = val.value.length;
        len > limit ? val.value = val.value.substring(0, limit) : $(countTxt).text(limit - len);
    },

    /* RELÓGIO */
    getDate: function (elem) {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();

        if (s < 10) {
            s = "0" + s;
        }

        $(elem).text(h + ":" + m + ":" + s);
        setTimeout(function () { getdate() }, 1000);
    },

    /** DATA */
    datePicker: function (elem, multi) {
        $(elem).on("apply.daterangepicker", function (e, picker) {
            picker.element.val(picker.startDate.format(picker.locale.format));
        });

        var elem = $(elem);
        var optionSelect = multi;

        if (!optionSelect == "") {
            optionSelect = false;
        } else {
            optionSelect = true;
        }

        $(elem).daterangepicker({
            locale: {
                format: 'DD/MM/YY',
                "daysOfWeek": [
                    "S",
                    "T",
                    "Q",
                    "Q",
                    "S",
                    "S",
                    "D"
                ],
                "monthNames": [
                    "Janeiro",
                    "Fevereiro",
                    "Março",
                    "Abril",
                    "Maio",
                    "Junho",
                    "Julho",
                    "Agosto",
                    "Setembro",
                    "Outubro",
                    "Novembro",
                    "Dezembro"
                ],
            },
            "firstDay": 1,
            //autoUpdateInput: false,
            singleDatePicker: optionSelect,
            autoUpdateInput: false,
            "applyClass": "btn-primary"
        });
    },

    /** MASCARAS */
    masks: function () {
        var CpfCnpj = {
            onKeyPress: function (cpf, ev, el, op) {
                var masks = ['000.000.000-000', '00.000.000/0000-00'],
                    mask = (cpf.length > 14) ? masks[1] : masks[0];
                el.mask(mask, op);
            }
        }

        var cellPhone = {
            placeholder: '(00) 0000-0000',
            focusout: function (phoneNumber, ev, el, op) {
                var masks = ['(99) 99999-9999', '(99) 99999-9999'],
                    mask = (phoneNumber.length > 10) ? masks[0] : masks[1];
                el.mask(mask, op);
            }
        }

        $('.money').mask('000.000.000.000.000,00', { reverse: true, placeholder: "R$ 0,00" });
        $(".cpfcnpj").mask('000.000.000-000', CpfCnpj);
        $('.dates').mask('00/00/0000', { placeholder: "__/__/____" });
        $('.cep').mask('00000-000', { placeholder: "00000-000" });
        $('.phone').mask("(99) 9999-99999", cellPhone);
        $('.time').mask('00:00', { placeholder: "00:00" });
    },

    /** AUTOCOMPLETE SELECT */

    autoComplete: function (elem) {
        $(elem).chosen({
            no_results_text: "Não encontrado",
            placeholder_text_multiple: "Selecione"
        });
    },

    /** AUTOCOMPLETE MULTIPLE SELECT */
    autoCompleteMulti: function (elem) {
        $(elem).chosen({
            no_results_text: "Não encontrado",
            placeholder_text_multiple: "Selecione",
            width: "100%"
        });
    },

    /** GALERIA DE IMAGENS COM MODAL */
    imageGallery: function (elem, galleryMinSlides, galleryMaxSlides, galleryMargin, pagination) {

        galleryMinSlides == null || galleryMinSlides == '' ? galleryMinSlides == 1 : galleryMinSlides == galleryMinSlides;
        galleryMaxSlides == null || galleryMaxSlides == '' ? galleryMaxSlides == 10 : galleryMaxSlides == galleryMaxSlides;
        //galleryWidth == null || galleryWidth == '' ? galleryWidth == '100%' : galleryWidth == galleryWidth;
        galleryMargin == null || galleryMargin == '' ? galleryMargin == 10 : galleryMargin == galleryMargin;
        //responsive == null || responsive == '' ? responsive == true : responsive == responsive;
        pagination == null || pagination == '' ? pagination == true : pagination == pagination;

        $(elem).bxSlider({
            minSlides: galleryMinSlides,
            maxSlides: galleryMaxSlides,
            //slideWidth: galleryWidth,
            slideMargin: galleryMargin,
            adaptiveHeight: true,
            responsive: true,
            pager: pagination
        });

        lightGallery(document.getElementById('lightgallery'));
    },

    fixBar: function (elem, btn) {
        window.onscroll = function () { myFunction(); };

        var header = elem.replace('#', '');

        //console.log(header)

        header = document.getElementById(header);
        var sticky = header.offsetTop;

        function myFunction() {
            if (window.pageYOffset >= sticky) {
                header.classList.add("sticky");
            } else {
                header.classList.remove("sticky");
            }
        }

        omni.geral.buttonTop(btn);
    },

    buttonTop: function (elem) {
        $(elem).on('click', function (e) {
            e.preventDefault();
            var target = $(this).attr('href');
            $('html, body').animate({ scrollTop: $(target).offset().top - 80 }, 1000);

        });
    },

    validation: function (elem) {
        $(elem).validator();
    }


};

/* TABELAS */
omni.tabelas = omni.tabelas || {};

omni.tabelas = {

    zeroTable: function (elem) {
        $(elem).DataTable({
            responsive: true,
            "oLanguage": aoLanguage,
            "bProcessing": true,
            "paging": false,
            "ordering": false,
            "info": false,
            "searching": false,
            //'select': true,

            columnDefs: [{
                orderable: false,
                className: 'no-sort',
                targets: [0]
            }],

            /* AO FILTRAR */
            "drawCallback": function (settings) {
                $('[data-toggle="tooltip"]').tooltip(); // your tootlip function.
                $('[data-toggle="popover"]').popover({
                    trigger: 'hover',
                    placement: 'top'
                });
            },

            /* AO INICIALIZAR */
            "fnInitComplete": function () {
            }
        });
    },

    simpleTable: function (elem, order) {

        if (order == "" || order == null) {
            order == [0, 1, 7, 8]
        }

        $(elem).DataTable({
            responsive: true,
            "oLanguage": aoLanguage,
            "bProcessing": true,

            columnDefs: [{
                orderable: false,
                className: 'no-sort',
                targets: [order]
            }],

            /* AO FILTRAR */
            "drawCallback": function (settings) {
                $('[data-toggle="tooltip"]').tooltip(); // your tootlip function.
                $('[data-toggle="popover"]').popover({
                    trigger: 'hover',
                    placement: 'top'
                });
            },

            /* AO INICIALIZAR */
            "fnInitComplete": function () {
            }
        });

    },

    scrollTable: function (elem, size) {

        $(elem).DataTable({
            responsive: true,
            "oLanguage": aoLanguage,
            "bProcessing": true,

            //SCROLL
            scrollY: size,
            scrollCollapse: true,
            paging: false,

            /* AO FILTRAR */
            "drawCallback": function (settings) {
                $('[data-toggle="tooltip"]').tooltip(); // your tootlip function.
                $('[data-toggle="popover"]').popover({
                    trigger: 'hover',
                    placement: 'top'
                });
            },

            /* AO INICIALIZAR */
            "fnInitComplete": function () {
            }
        });

    },

    // FUNÇÃO - DATATABLESGROUP('#ID-DO-ELEMENTO','NÚMERO DA COLUNA QUE FORMARÁ O GRUPO','QUANTIDADE DE COLUNAS DA TABELA',COLUNA QUE IRÁ ORDENAR (GERALMENTE A 0))
    groupTable: function (elem, colNumber, columnsQut, orderColumn) {
        var elem = $(elem);
        var colNumber = colNumber;
        var columnsQut = String(columnsQut);
        var orderColumn = String(orderColumn);

        var table = elem.DataTable({
            "oLanguage": aoLanguage,
            responsive: true,
            "columnDefs": [
                { "visible": false, "targets": colNumber }
            ],
            "order": [
                [orderColumn, 'asc']
            ],
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows({ page: 'current' }).nodes();
                var last = null;

                api.column(colNumber, { page: 'current' }).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                            '<tr class="group"><td colspan="' + columnsQut + '">' + group + '</td></tr>'
                        );

                        last = group;
                    }
                });
            }
        });

        // Order by the grouping
        elem.on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                table.order([colNumber, 'desc']).draw();
            } else {
                table.order([colNumber, 'asc']).draw();
            }
        });

    }

};


/* INICIO GERAL */

jQuery(function ($) {

    /*ToolTip e popover */
    $('[data-rel=tooltip], .toolInput, .tooltip').tooltip({ trigger: "focus" });
    $('[data-toggle=popover]').popover({ html: true, trigger: 'hover' });

    //Hover in button group
    $(".btn-group > .btn").click(function () {
        $(this).parent().find('.btn').removeClass("active");
        $(this).addClass("active");
    });

    //ABAS
    $('.panel-title a').on('click', function (e) {
        e.preventDefault();
        var elem = $(this).parents(':eq(2)').find('.panel-collapse');
        if ($(this).parents(':eq(2)').find('a:after').hasClass('in')) {
            $(this).find('i').toggleClass('fa-flip-vertical');
        } else {
            $(this).find('i').toggleClass('fa-flip-vertical');
        }
    });

    omni.tabelas.simpleTable('.table', [0]);


});