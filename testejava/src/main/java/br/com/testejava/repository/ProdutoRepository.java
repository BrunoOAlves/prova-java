package br.com.testejava.repository;

import br.com.testejava.model.ProdutoX;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProdutoRepository extends JpaRepository<ProdutoX, Long>{
}
