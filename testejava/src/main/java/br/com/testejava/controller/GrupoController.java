package br.com.testejava.controller;

import br.com.testejava.model.Grupo;
import br.com.testejava.model.ProdutoX;
import br.com.testejava.repository.GrupoRepository;
import br.com.testejava.repository.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

@Controller
public class GrupoController {

    @Autowired
    private GrupoRepository gr;

    @Autowired
    private ProdutoRepository pr;

    //Get da index
    @GetMapping("/")
        public String index(Model model) {
        model.addAttribute("grupos", gr.findAll());
        return "index";
    }

    //Get Listar os produtos no Formulario
    @GetMapping("/cadastrar")
    public String listaProdutos(Model model){
        List<ProdutoX> produtos = pr.findAll();
        model.addAttribute("produtos", produtos);
        return "formgrupo";
    }

    //Post para salvar o grupo
    @PostMapping("/cadastrar")
    public String salvaGrupo(Grupo grupo, RedirectAttributes attributes){
        List<ProdutoX> produtos = grupo.getProduto();
        gr.save(grupo);

        attributes.addFlashAttribute("cadastrosucesso", "Grupo criado com sucesso.");

        return "redirect:/";
    }

    //get para visualizar grupo
    @GetMapping("/visualizar/{id}")
    public String visualizar(@PathVariable("id") Long id, Model model) {
        Optional<Grupo> getGrupoId = gr.findById(id);
        if (getGrupoId.isPresent()) {
            List<ProdutoX> produtosUsados = new ArrayList<>(getGrupoId.get().getProduto());
            List<ProdutoX> produtosDisponiveis = new ArrayList<>();
            List<ProdutoX> produtos = pr.findAll();
            produtos.forEach(p1 -> {
                AtomicBoolean igual = new AtomicBoolean(false);
                produtosUsados.forEach(p2 -> {
                    if (p1.getIdProduto().equals(p2.getIdProduto()))
                        igual.set(true);
                });
                if (!igual.get())
                    produtosDisponiveis.add(p1);
            });
            model.addAttribute("grupo", getGrupoId.get() );
            model.addAttribute("produtosUsados", produtosUsados);
            model.addAttribute("produtos", produtosDisponiveis);
            return "visualizacao";
        } else {
            return "redirect:/";
        }
    }

    //Get para deletar id
    @GetMapping("/excluir/{id}")
    public String deletarGrupo(@PathVariable("id") Long id, RedirectAttributes attributes){
        gr.deleteById(id);

        attributes.addFlashAttribute("deletesucesso", "Registro removido com sucesso");

        return "redirect:/";
    }


}
