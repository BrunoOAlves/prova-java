package br.com.testejava.model;

import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "Grupo")
public class Grupo {

    @Id
    @GeneratedValue(generator = "grupo_id")
    @SequenceGenerator(name="grupo_id", sequenceName = "grupo_id", allocationSize = 1)
    @Column(name="id_grupo")
    private Long id;

    @Column(name = "nome_grupo")
    private String nomeGrupo;

    @ManyToMany
    private List<ProdutoX> produto;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeGrupo() {
        return nomeGrupo;
    }

    public void setNomeGrupo(String nomeGrupo) {
        this.nomeGrupo = nomeGrupo;
    }

    public List<ProdutoX> getProduto() {
        return produto;
    }

    public void setProduto(List<ProdutoX> produto) {
        this.produto = produto;
    }
}
