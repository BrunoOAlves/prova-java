package br.com.testejava.model;

import javax.persistence.*;
import java.io.Serializable;

@Table(name = "produtos")
@Entity
public class ProdutoX implements Serializable {

    @Id
    @GeneratedValue(generator = "produto_id")
    @SequenceGenerator(name="produto_id", sequenceName = "produto_id", allocationSize = 1)
    @Column(name = "id_produto")
    private Long idProduto;

    @Column(name = "nome_produto")
    private String nomeProduto;

    @Override
    public String toString() {
        return nomeProduto;
    }

    public Long getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(long idProduto) {
        this.idProduto = idProduto;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }
}
